package pages.employee_details;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.testng.Assert;
import utils.TestUtils;

public class EmployeeReportToPage extends EmployeeDetailsModulesPageBase {

    public static class ReportingMethods {
        public static final String DIRECT = "Direct";
        public static final String INDIRECT = "Indirect";
        public static final String OTHER = "Other";
    }

    public static String urlModulePath = "viewReportToDetails/";

    public EmployeeReportToPage(WebDriver driver) {
        super(driver);
        System.out.println("Init Employee Details Report-To page");
    }

    @FindBy(id = "btnAddSupervisorDetail")
    private WebElement btnAddSupervisor;

    @FindBy(id = "btnAddSubordinateDetail")
    private WebElement btnAddSubordinate;

    @FindBy(id = "btnSaveReportTo")
    private WebElement btnSave;

    @FindBy(id = "reportto_supervisorName_empName")
    private WebElement txtSupervisorName;

    @FindBy(id = "reportto_reportingMethodType")
    private WebElement ddlReportMethod;

    public void clickAddSupervisor(){
        WebElement addButton = waitUntilClickable(btnAddSupervisor);
        int maxAttempts = 5;
        int currentAttempts = 0;

        do {
            addButton.click();
            currentAttempts++;
        } while (!TestUtils.isVisible(btnSave, driver, 5) && currentAttempts < maxAttempts);

        Assert.assertTrue(TestUtils.isVisible(btnSave, driver, 5));
    }

    public void inputSupervisorName(String name) {
        inputText(name, waitUntilClickable(txtSupervisorName));
    }

    public void selectReportingMethod(String method) {
        selectFromDropdown(waitUntilClickable(ddlReportMethod), method);
    }

    public void save(){
        click(waitUntilClickable(btnSave));
    }

    public void saveSupervisor(String name, String reportMethod){
        inputSupervisorName(name);
        selectReportingMethod(reportMethod);
        int maxAttempts = 5;
        int currentAttempts = 0;
        do {
            save();
            currentAttempts++;
        } while (!TestUtils.isVisible(btnAddSupervisor, driver, 5)
                && currentAttempts < maxAttempts);

        Assert.assertTrue(TestUtils.isVisible(btnAddSupervisor, driver, 5),
                "Clicked Save button " + currentAttempts + " times but Add Supervisor button still not visible");
    }

}
