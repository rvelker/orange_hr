package pages.employee_details;

import org.openqa.selenium.WebDriver;

public class EmployeePersonalDetailsPage extends EmployeeDetailsModulesPageBase {

    public static String[] urlModulePath = {"viewPersonalDetails/","viewEmployee/"};

    public EmployeePersonalDetailsPage(WebDriver driver) {
        super(driver);
        System.out.println("Init Employee Personal Details page");
    }
}
