package pages.employee_details;

import org.openqa.selenium.WebDriver;

public class EmployeeContactDetailsPage extends EmployeeDetailsModulesPageBase {

    public static String urlModulePath = "contactDetails/";

    public EmployeeContactDetailsPage(WebDriver driver) {
        super(driver);
        System.out.println("Init Employee Contact Details Page");
    }
}
