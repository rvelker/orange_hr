package pages.employee_details;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.testng.Assert;
import utils.TestUtils;

public class EmployeeJobPage extends EmployeeDetailsModulesPageBase {

    public static String urlModulePath = "viewJobDetails";

    public EmployeeJobPage(WebDriver driver) {
        super(driver);
        System.out.println("Init Employee Job Page");
    }

    @FindBy(id = "btnSave")
    private WebElement btnEdit_Save;

    @FindBy(id = "job_job_title")
    public WebElement ddlJobTitle;

    @FindBy(id = "job_emp_status")
    public WebElement ddlEmploymentStatus;

    @FindBy(id = "job_sub_unit")
    public WebElement ddlSubUnit;

    public void selectJobTitle(String value) {
        selectFromDropdown(ddlJobTitle, value);
    }

    public void selectEmploymentStatus(String value) {
        selectFromDropdown(ddlEmploymentStatus, value);
    }

    public void selectSubUnit(String value) {
        selectFromDropdown(ddlSubUnit, value);
    }

    public void edit() {
        WebElement button = waitUntilClickable(btnEdit_Save);
        Assert.assertEquals(button.getAttribute("value"), "Edit",
                "Button was expected to read 'Edit' but actually reads " + button.getAttribute("value"));

        int maxAttempts = 5;
        int currentAttempts = 0;

        do {
            button.click();
            currentAttempts++;
        }
        while (!TestUtils.elementContainsText("Save", button, driver) && currentAttempts < maxAttempts);

        Assert.assertEquals(btnEdit_Save.getAttribute("value"), "Save",
                "Clicked 'Edit' button " + currentAttempts + " times but did not change into 'Save' button.");
    }

    public void save() {
        WebElement button = waitUntilClickable(btnEdit_Save);
        Assert.assertEquals(button.getAttribute("value"), "Save",
                "Button was expected to read 'Save' but actually reads " + button.getAttribute("value"));

        int maxAttempts = 5;
        int currentAttempts = 0;

        do {
            button.click();
            currentAttempts++;
        }
        while (!TestUtils.elementContainsText("Edit", button, driver) && currentAttempts < maxAttempts);

        Assert.assertEquals(btnEdit_Save.getAttribute("value"), "Edit",
                "Clicked 'Save' button " + currentAttempts + " times but did not change into 'Edit' button.");
    }

}
