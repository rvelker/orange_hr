package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import pages.page_base.PageBase;

public class LoginPage extends PageBase {

    public static String urlModulePath = "login";

    public LoginPage(WebDriver driver) {
        super(driver);
        System.out.println("Init Login Page");
        this.url = "https://opensource-demo.orangehrmlive.com/index.php/auth/login";
    }

    @FindBy(id = "txtUsername")
    private WebElement txtUsername;

    @FindBy(id = "txtPassword")
    private WebElement txtPassword;

    @FindBy(id = "btnLogin")
    private WebElement btnLogin;

    public void inputUsername(String username) {
        inputText(username, waitUntilClickable(txtUsername));
    }

    public void inputPassword(String password) {
        inputText(password, waitUntilClickable(txtPassword));
    }

    public void login(String user, String pass){
        inputUsername(user);
        inputPassword(pass);
        waitUntilClickable(btnLogin).click();
    }

    public void adminLogin(){
        String adminUsername = "Admin";
        String adminPassword = "admin123";
        inputUsername(adminUsername);
        inputPassword(adminPassword);
        waitUntilVisible(btnLogin).click();
    }

}
