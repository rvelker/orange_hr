package pages;

import org.openqa.selenium.WebDriver;
import pages.page_base.PageBase;

public class DashboardPage extends PageBase {

    public static String urlModulePath = "dashboard";

    public DashboardPage(WebDriver driver) {
        super(driver);
        System.out.println("Init Dashboard Page");
        this.url = "https://opensource-demo.orangehrmlive.com/index.php/dashboard";
    }



}
