package pages.admin_job.jobTitles;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.testng.Assert;
import pages.page_base.PageBase;
import utils.TestUtils;

public class AdminJobTitlesPage extends PageBase {

    public static String urlModulePath = "viewJobTitleList";

    public AdminJobTitlesPage(WebDriver driver) {
        super(driver);
        System.out.println("Init Admin Job Titles Page");
    }

    @FindBy(id = "btnAdd")
    private WebElement btnAdd;

    public AdminSaveJobTitlesPage addJob() {
        WebElement btn = waitUntilClickable(btnAdd);
        int maxAttempts = 5;
        int currentAttempts = 0;

        do {
            btn.click();
            currentAttempts++;
        } while (!TestUtils.waitForUrlToContain(AdminSaveJobTitlesPage.urlModulePath, driver, 5)
                && currentAttempts < maxAttempts);

        Assert.assertTrue(TestUtils.waitForUrlToContain(AdminSaveJobTitlesPage.urlModulePath, driver, 5),
                "Clicked 'Add' button " + currentAttempts + " times but did not open Add Job Title form.");
        return new AdminSaveJobTitlesPage(driver);
    }
}
