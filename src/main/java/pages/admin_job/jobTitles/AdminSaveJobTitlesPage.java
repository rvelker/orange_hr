package pages.admin_job.jobTitles;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import pages.page_base.PageBase;

public class AdminSaveJobTitlesPage extends PageBase {

    public static String urlModulePath = "saveJobTitle";

    public AdminSaveJobTitlesPage(WebDriver driver) {
        super(driver);
        System.out.println("Init Admin Save Job Titles Page");
    }

    @FindBy(id = "jobTitle_jobTitle")
    private WebElement txtJobTitle;

    @FindBy(id = "jobTitle_jobDescription")
    private WebElement txtJobDescription;

    @FindBy(id = "jobTitle_note")
    private WebElement txtNote;

    @FindBy(id = "btnSave")
    private WebElement btnSave;

    public void inputJobTitle(String title) {
        inputText(title,waitUntilClickable(txtJobTitle));
    }

    public void inputJobDescription(String description) {
        inputText(description,waitUntilClickable(txtJobDescription));
    }

    public void inputJobNote(String note) {
        inputText(note,waitUntilClickable(txtNote));
    }

    public void clickSave() {
        click(btnSave);
    }

    public void saveJob(String title) {
        inputJobTitle(title);
        clickSave();
    }
}
