package pages;

import org.openqa.selenium.*;
import org.openqa.selenium.support.FindBy;
import org.testng.Assert;
import pages.page_base.PageBase;
import pages.employee_details.EmployeePersonalDetailsPage;
import utils.TestUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class EmployeeListPage extends PageBase {

    public static String urlModulePath = "viewEmployeeList";

    public static class TableColumns {
        public static final int totalColumns = 8;
        public static final int SELECTED = 0;
        public static final int ID = 1;
        public static final int FIRST_NAME = 2;
        public static final int LAST_NAME = 3;
        public static final int JOB = 4;
        public static final int STATUS = 5;
        public static final int SUB_UNIT = 6;
        public static final int SUPERVISOR = 7;
    }

    public EmployeeListPage(WebDriver driver) {
        super(driver);
        System.out.println("Init Employee List Page");
        this.url = "https://opensource-demo.orangehrmlive.com/index.php/pim/viewEmployeeList";
    }

    //region PAGINATION ELEMENTS
    @FindBy(className = "paging top")
    private WebElement paginationBarTop;

    @FindBy(css = "#frmList_ohrmListComponent > div.top > ul > li.desc")
    private WebElement paginationText;

    @FindBy(css = "#frmList_ohrmListComponent > div.bottom > ul > li.next > a")
    private WebElement btnNext;
    //endregion

    //region SEARCH PANEL
    @FindBy(id = "searchBtn")
    private WebElement btnSearch;

    @FindBy(id = "empsearch_employee_name_empName")
    private WebElement txtEmployeeName;

    @FindBy(id = "empsearch_id")
    private WebElement txtEmployeeId;
    //endregion

    @FindBy(css = "#resultTable > tbody")
    public WebElement tblEmployees;

    public void searchEmployeeByName(String name) {
        try {
            inputText(name, waitUntilClickable(txtEmployeeName));
        } catch (AssertionError ignored) {
            inputText(name, waitUntilClickable(txtEmployeeName));
        }
        click(btnSearch);
    }

    public void searchEmployeeById(String id) {
        inputText(id, txtEmployeeId);
        click(btnSearch);
    }

    public void openEmployeeDetailsPage(String employeeId) {
        int maxAttempts = 5;
        int currentAttempts = 0;
        WebElement employeeIdAnchorTag = waitUntilClickable(driver.findElement(By.linkText(employeeId)));

        do {
            employeeIdAnchorTag.click();
            currentAttempts++;
        } while (!TestUtils.waitForUrlToContain(EmployeePersonalDetailsPage.urlModulePath[1], driver, 10)
                && currentAttempts < maxAttempts);

        Assert.assertTrue(TestUtils.waitForUrlToContain(EmployeePersonalDetailsPage.urlModulePath[1], driver, 10),
                "Clicked employee id anchor tag " + currentAttempts + " times but URL did not change to View Personal Details page");
    }

    public boolean isPaginated() {
        try {
            waitUntilVisible(paginationBarTop);
        } catch (NoSuchElementException | TimeoutException e) {
            return false;
        }
        return true;
    }

    public void nextList(){
        click(btnNext);
    }

    public boolean isOnLastList() {
        String text = waitUntilVisible(paginationText).getText();
        Pattern p = Pattern.compile("\\d+");
        Matcher m = p.matcher(text);

        List<String> employeeNumbers = new ArrayList<>();

        while(m.find()) {
            employeeNumbers.add(m.group());
        }
        int lastEmployeeOnCurrentPage = Integer.parseInt(employeeNumbers.get(1));
        int lastEmployeeInList = Integer.parseInt(employeeNumbers.get(2));

        return lastEmployeeOnCurrentPage == lastEmployeeInList;
    }
}
