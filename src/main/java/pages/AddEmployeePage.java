package pages;

import data_models.Employee;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.testng.Assert;
import pages.page_base.PageBase;
import pages.employee_details.EmployeePersonalDetailsPage;
import utils.TestUtils;

public class AddEmployeePage extends PageBase {

    public static String urlModulePath = "addEmployee";

    public AddEmployeePage(WebDriver driver) {
        super(driver);
        System.out.println("Init Add Employee Page");
        this.url = "https://opensource-demo.orangehrmlive.com/index.php/pim/addEmployee";
    }

    //region WEB ELEMENTS
    @FindBy(id = "firstName")
    private WebElement txtFirstName;

    @FindBy(id = "middleName")
    private WebElement txtMiddleName;

    @FindBy(id = "lastName")
    private WebElement txtLastName;

    @FindBy(id = "employeeId")
    private WebElement txtEmployeeId;

    @FindBy(id = "chkLogin")
    private WebElement chkCreateLoginDetails;

    @FindBy(id = "user_name")
    private WebElement txtUsername;

    @FindBy(id = "user_password")
    private WebElement txtPassword;

    @FindBy(id = "re_password")
    private WebElement txtConfirmPassword;

    @FindBy(id = "status")
    private WebElement ddlStatus;

    @FindBy(id = "btnSave")
    private WebElement btnSave;
    //endregion

    public void addNewEmployee(Employee employee) {
        inputText(employee.getFirstName(), waitUntilClickable(txtFirstName));
        inputText(employee.getLastName(), waitUntilClickable(txtLastName));
        inputText(employee.getId(), waitUntilClickable(txtEmployeeId));

        if (employee.getMiddleName() != null) {
            inputText(employee.getMiddleName(), waitUntilClickable(txtMiddleName));
        }

        if (employee.hasLoginDetails()) {
            clickLoginDetailsCheckbox();
            inputText(employee.getUsername(), waitUntilClickable(txtUsername));
            inputText(employee.getPassword(), waitUntilClickable(txtPassword));
            inputText(employee.getPassword(), waitUntilClickable(txtConfirmPassword));

            if (employee.isStatusEnabled()) {
                selectStatusEnabled();
            } else {
                selectStatusDisabled();
            }
        }
        int maxAttempts = 5;
        int currentAttempts = 0;

        do {
            clickSave();
            currentAttempts++;
        } while (!TestUtils.waitForUrlToContain(EmployeePersonalDetailsPage.urlModulePath[0], driver,10)
                && currentAttempts < maxAttempts);

        Assert.assertTrue(TestUtils.waitForUrlToContain(EmployeePersonalDetailsPage.urlModulePath[0], driver,10),
                "Clicked save button " + currentAttempts + " times but URL did not change to View Personal Details page.");
    }

    public void clickLoginDetailsCheckbox(){
        waitUntilClickable(chkCreateLoginDetails).click();
    }

    public void selectStatusEnabled(){
        selectFromDropdown(ddlStatus, "Enabled");
    }

    public void selectStatusDisabled(){
        selectFromDropdown(ddlStatus, "Disabled");
    }

    public void clickSave(){
        waitUntilClickable(btnSave).click();
    }


}
