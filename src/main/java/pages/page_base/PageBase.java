package pages.page_base;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import utils.TestUtils;

public class PageBase {

    protected WebDriver driver;
    protected WebDriverWait wait;
    public String url;

    public PageBase(WebDriver driver){
        this.driver = driver;
        wait = new WebDriverWait(driver, 20);
        PageFactory.initElements(driver, this);
    }

    protected WebElement waitUntilVisible(WebElement element){
        return wait.until(ExpectedConditions.visibilityOf(element));
    }

    protected WebElement waitUntilClickable(WebElement element){
        return wait.until(ExpectedConditions.elementToBeClickable(element));
    }

    public void inputText(String value, WebElement field){
        waitUntilClickable(field);

        int maxAttempts = 7;
        int currentAttempt = 0;

        do {
            currentAttempt ++;
            field.clear();
            field.sendKeys(value);
        } while (!field.getAttribute("value").equals(value) && currentAttempt < maxAttempts);

        Assert.assertEquals(field.getAttribute("value"), value,
                "Value displayed in field different from input");
    }

    public void click(WebElement element) {
        TestUtils.clickElement(element, driver);
    }

    public void logout() {
        waitUntilClickable(driver.findElement(By.id("welcome"))).click();
        waitUntilClickable(driver.findElement(By.cssSelector("#welcome-menu > ul > li:nth-child(3) > a"))).click();
    }

    public void selectFromDropdown(WebElement dropdown, String value) {
        waitUntilClickable(dropdown);
        Select select = new Select(dropdown);
        select.selectByVisibleText(value);
    }

}
