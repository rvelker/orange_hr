package utils;

import org.openqa.selenium.*;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import java.util.ArrayList;
import java.util.List;

public class TestUtils {

    public static void waitUntilTextFieldIsPopulated(WebElement element, WebDriver driver) {
        new WebDriverWait(driver, 5).until(new ExpectedCondition<Boolean>() {
            public Boolean apply(WebDriver d) {
                return element.getAttribute("value").length() != 0;
            }
        });
    }

    public static Boolean elementContainsText(String text, WebElement element, WebDriver driver) {
        WebDriverWait wait = new WebDriverWait(driver, 5);
        WebElement elem = waitForVisibility(element, driver);

        try {
            wait.until(ExpectedConditions.textToBePresentInElementValue(elem, text));
            return true;
        } catch (TimeoutException e) {
            try {
                wait.until(ExpectedConditions.textToBePresentInElement(elem, text));
                return true;
            } catch (TimeoutException e1) {
                return false;
            }
        }
    }


    public static WebElement getParentOfElement(WebElement element, WebDriver driver) {
        return waitForVisibility(element.findElement(By.xpath("./..")), driver);
    }

    public static List<WebElement> getChildrenOfElement(WebElement element, WebDriver driver) {
        return new ArrayList<>(element.findElements(By.xpath("./child::*")));
    }

    public static WebElement getNextSibling(WebElement element, WebDriver driver) {
        return waitForVisibility(element.findElement(By.xpath("following-sibling::*")), driver);
    }


    public static WebElement waitForVisibility(WebElement element, WebDriver driver) {
        WebDriverWait wait = new WebDriverWait(driver, 20);
        return wait.until(ExpectedConditions.visibilityOf(element));
    }

    public static WebElement waitForClickable(WebElement element, WebDriver driver) {
        WebDriverWait wait = new WebDriverWait(driver, 30);
        return wait.until(ExpectedConditions.elementToBeClickable(element));
    }


    public static boolean isVisible(WebElement element, WebDriver driver, int timeout) {
        WebDriverWait wait = new WebDriverWait(driver, timeout);
        try {
            wait.until(ExpectedConditions.visibilityOf(element));
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public static boolean isClickable(WebElement element, WebDriver driver, int timeout) {
        WebDriverWait wait = new WebDriverWait(driver, timeout);
        try {
            wait.until(ExpectedConditions.elementToBeClickable(element));
            return true;
        } catch (Exception e) {
            return false;
        }
    }


    public static boolean waitForUrlToBe(String url, WebDriver driver, int timeout) {
        WebDriverWait wait = new WebDriverWait(driver, timeout);
        try {
            wait.until(ExpectedConditions.urlToBe(url));
            return true;
        } catch (TimeoutException e) {
            return false;
        }
    }

    public static boolean waitForUrlToContain(String string, WebDriver driver, int timeout) {
        WebDriverWait wait = new WebDriverWait(driver, timeout);
        try {
            wait.until(ExpectedConditions.urlContains(string));
            return true;
        } catch (TimeoutException e) {
            return false;
        }
    }


    public static void clickElement(WebElement element, WebDriver driver) {
        WebElement elem = waitForClickable(element, driver);
        elem.click();
    }

}
