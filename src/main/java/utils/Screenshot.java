package utils;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.Date;

public class Screenshot {

    public static void takeScreenshot(WebDriver driver, String testPath, String testMethod, Object[] parameters) {
        String fileName = screenshotName(parameters);
        String destination = createFoldersAndReturnPath(testPath, testMethod);
        createScreenshotFile(driver, destination + fileName);
    }

    private static String screenshotName(Object[] parameters) {
        StringBuilder fileName = new StringBuilder();

        if (parameters != null) {

            for (int i = 0; i < parameters.length; i++) {

                String param = (String) parameters[i];

                if (!param.equals("")) {
                    fileName.append(param);

                    if (i != parameters.length - 1) {
                        fileName.append(" ");
                    }
                }
            }

        } else {
            fileName.append("screenshot");
        }

        fileName.append(" - ")
                .append(getTime());

        return fileName.toString();
    }

    private static String createFoldersAndReturnPath(String testPath, String testMethod) {
        String rootFolder = "D:\\Selenium Screenshots\\";
        StringBuilder folderPath = new StringBuilder().append(rootFolder);
        String[] splitTestPath = testPath.split("\\.");

        for (String folderName : splitTestPath) {
            folderPath.append(folderName).append("\\");
            createFolder(folderPath.toString());
        }

        String date = getDate();
        folderPath.append(date)
                .append("\\");
        createFolder(folderPath.toString());


        folderPath.append(testMethod)
                .append("\\");
        createFolder(folderPath.toString());


        return folderPath.toString();
    }

    private static void createFolder(String folderPath) {
        File folder = new File(folderPath);

        if (!folder.exists()) {

            if (folder.mkdir()) {
                System.out.println("Created screenshot test class folder " + folder);
            } else {
                System.out.println("Error creating screenshot test class folder " + folder);
            }
        }
    }

    private static void createScreenshotFile(WebDriver driver, String imagePath) {
        try {
            File scrFile = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
            FileUtils.copyFile(scrFile, new File(imagePath));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static String getDate() {
        return LocalDate.now().toString();
    }

    private static String getTime() {
        long currentTimeMillis = System.currentTimeMillis();
        Date instant = new Date(currentTimeMillis);
        SimpleDateFormat sdf = new SimpleDateFormat("HH:mm");
        return sdf.format(instant).replace(":", "-");
    }

}
