package navigation;

public class NavMenus {

    public static class MainMenus {
        public static final String ADMIN = "admin";
        public static final String PIM = "pim";
        public static final String LEAVE = "leave";
        public static final String TIME = "time";
    }

    public static class SecondaryMenus {

        public static class Admin {
            public static final String USER_MANAGEMENT = "admin.user management";
            public static final String JOB =  "admin.job";
            public static final String ORGANIZATION = "admin.organization";
            public static final String QUALIFICATIONS = "admin.qualifications";
            public static final String NATIONALITIES = "admin.nationalities";
            public static final String CONFIGURATION = "admin.configuration";
        }

        public static class Pim {
            public static final String CONFIGURATION = "pim.configuration";
            public static final String EMPLOYEE_LIST = "pim.employee list";
            public static final String ADD_EMPLOYEE = "pim.add employee";
            public static final String REPORTS = "pim.reports";
        }
    }

    public static class TertiaryMenus {

        public static class Admin_Job {
            public static final String JOB_TITLES = "admin.job.job titles";
            public static final String PAY_GRADES = "admin.job.pay grades";
            public static final String EMPLOYMENT_STATUS = "admin.job.employment status";
            public static final String JOB_CATEGORIES = "admin.job.job categories";
            public static final String WORK_SHIFTS = "admin.job.work shifts";
        }
    }


}
