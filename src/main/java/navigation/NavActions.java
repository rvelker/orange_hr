package navigation;

import org.openqa.selenium.*;
import org.openqa.selenium.interactions.Actions;
import org.testng.Assert;
import pages.AddEmployeePage;
import pages.EmployeeListPage;
import pages.admin_job.jobTitles.AdminJobTitlesPage;
import utils.TestUtils;

public class NavActions {

    public static class Base {
        private static void selectMainMenu(WebDriver driver, String menu) {
            WebElement target;

            switch (menu) {

                case NavMenus.MainMenus.ADMIN:
                    target = driver.findElement(By.id("menu_admin_viewAdminModule"));
                    break;

                case NavMenus.MainMenus.PIM:
                    target = driver.findElement(By.id("menu_pim_viewPimModule"));
                    break;

                case NavMenus.MainMenus.LEAVE:
                    target = driver.findElement(By.id("menu_leave_viewLeaveModule"));
                    break;

                case NavMenus.MainMenus.TIME:
                    target = driver.findElement(By.id("menu_time_viewTimeModule"));
                    break;

                default : throw new IllegalArgumentException("Menu name <" + menu + "> is not recognized");
            }
            Actions actions = new Actions(driver);
            actions.moveToElement(TestUtils.waitForClickable(target, driver)).perform();
        }

        private static void selectSubMenu(WebDriver driver, String menu) {
            WebElement target;

            switch(menu) {

                //region ADMIN
                case NavMenus.SecondaryMenus.Admin.USER_MANAGEMENT:
                    target = driver.findElement(By.id("menu_admin_UserManagement"));
                    break;

                case NavMenus.SecondaryMenus.Admin.JOB:
                    target = driver.findElement(By.id("menu_admin_Job"));
                    break;

                case NavMenus.SecondaryMenus.Admin.ORGANIZATION:
                    target = driver.findElement(By.id("menu_admin_Organization"));
                    break;

                case NavMenus.SecondaryMenus.Admin.QUALIFICATIONS:
                    target = driver.findElement(By.id("menu_admin_Qualifications"));
                    break;

                case NavMenus.SecondaryMenus.Admin.NATIONALITIES:
                    target = driver.findElement(By.id("menu_admin_nationality"));
                    TestUtils.waitForClickable(target, driver).click();
                    return;

                case NavMenus.SecondaryMenus.Admin.CONFIGURATION:
                    target = driver.findElement(By.id("menu_admin_Configuration"));
                    break;
                //endregion

                //region PIM
                case NavMenus.SecondaryMenus.Pim.CONFIGURATION:
                    target = driver.findElement(By.id("menu_pim_Configuration"));
                    break;

                case NavMenus.SecondaryMenus.Pim.EMPLOYEE_LIST:
                    target = driver.findElement(By.id("menu_pim_viewEmployeeList"));
                    TestUtils.waitForClickable(target, driver).click();
                    return;

                case NavMenus.SecondaryMenus.Pim.ADD_EMPLOYEE:
                    target = driver.findElement(By.id("menu_pim_addEmployee"));
                    TestUtils.waitForClickable(target, driver).click();
                    return;
                //endregion

                default:
                    throw new IllegalArgumentException("Menu name <" + menu + "> is not recognised");
            }
            Actions actions = new Actions(driver);
            actions.moveToElement(target).perform();
        }

        private static void selectTertiaryMenu(WebDriver driver, String menu) {
            WebElement target;

            switch (menu) {

                case NavMenus.TertiaryMenus.Admin_Job.JOB_TITLES:
                    target = driver.findElement(By.id("menu_admin_viewJobTitleList"));
                    TestUtils.waitForClickable(target, driver).click();
                    return;

                case NavMenus.TertiaryMenus.Admin_Job.PAY_GRADES:
                    target = driver.findElement(By.id("menu_admin_viewPayGrades"));
                    TestUtils.waitForClickable(target, driver).click();
                    return;

                case NavMenus.TertiaryMenus.Admin_Job.EMPLOYMENT_STATUS:
                    target = driver.findElement(By.id("menu_admin_employmentStatus"));
                    TestUtils.waitForClickable(target, driver).click();
                    return;

                case NavMenus.TertiaryMenus.Admin_Job.JOB_CATEGORIES:
                    target = driver.findElement(By.id("menu_admin_jobCategory"));
                    TestUtils.waitForClickable(target, driver).click();
                    return;

                case NavMenus.TertiaryMenus.Admin_Job.WORK_SHIFTS:
                    target = driver.findElement(By.id("menu_admin_workShift"));
                    TestUtils.waitForClickable(target, driver).click();
            }
        }
    }

    private static void safeNav(WebDriver driver, String menu, String secMenu, String thirdMenu) {
        // hacky fix for the flaky navigation bar interactions
        try {

            Base.selectMainMenu(driver, menu);

            if (secMenu != null) {

                Base.selectSubMenu(driver, secMenu);

                if (thirdMenu != null) {
                    Base.selectTertiaryMenu(driver, thirdMenu);
                }
            }

        } catch (Exception ignored) {

            Base.selectMainMenu(driver, menu);

            if (secMenu != null) {

                Base.selectSubMenu(driver, secMenu);

                if (thirdMenu != null) {
                    Base.selectTertiaryMenu(driver, thirdMenu);
                }
            }
        }
    }

    public static void goToAdminJobTitles(WebDriver driver) {
        safeNav(driver, NavMenus.MainMenus.ADMIN, NavMenus.SecondaryMenus.Admin.JOB, NavMenus.TertiaryMenus.Admin_Job.JOB_TITLES);
        Assert.assertTrue(TestUtils.waitForUrlToContain(AdminJobTitlesPage.urlModulePath, driver, 20),
                "Driver should be on 'View Job Titles' Page. URL should contain << " + AdminJobTitlesPage.urlModulePath + " >> but actual URL is: " + driver.getCurrentUrl());
    }

    public static void goToAddEmployeePage(WebDriver driver){
        safeNav(driver, NavMenus.MainMenus.PIM, NavMenus.SecondaryMenus.Pim.ADD_EMPLOYEE, null);
        Assert.assertTrue(TestUtils.waitForUrlToContain(AddEmployeePage.urlModulePath, driver, 20),
                "Driver should be on 'Add Employee' page. URL should contain << " + AddEmployeePage.urlModulePath + " >> but actual URL is: " + driver.getCurrentUrl());
    }

    public static void goToEmployeeListPage(WebDriver driver){
        safeNav(driver, NavMenus.MainMenus.PIM, NavMenus.SecondaryMenus.Pim.EMPLOYEE_LIST, null);
        Assert.assertTrue(TestUtils.waitForUrlToContain(EmployeeListPage.urlModulePath, driver, 20),
                "Driver should be on 'Employee List' page. URL should contain << " + EmployeeListPage.urlModulePath + " >> but actual URL is: " + driver.getCurrentUrl());
    }
}
