package navigation;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import pages.employee_details.EmployeeContactDetailsPage;
import pages.employee_details.EmployeeJobPage;
import pages.employee_details.EmployeePersonalDetailsPage;
import pages.employee_details.EmployeeReportToPage;
import utils.TestUtils;

import java.util.List;

public class NavEmployeeDetails {

    public static class Modules {
        private static final String PERSONAL_DETAILS = "Personal Details";
        private static final String CONTACT_DETAILS = "Contact Details";
        private static final String EMERGENCY_CONTACTS = "Emergency Contacts";
        private static final String DEPENDENTS = "Dependents";
        private static final String IMMIGRATION = "Immigration";
        private static final String JOB = "Job";
        private static final String SALARY = "Salary";
        private static final String TAX_EXEMPTIONS = "Tax Exemptions";
        private static final String REPORT_TO = "Report-to";
        private static final String QUALIFICATIONS = "Qualifications";
        private static final String MEMBERSHIPS = "Memberships";
    }


    private static final String navMenuId = "sidenav";

    private static void clickModuleButton(String moduleName, String urlModulePath, WebDriver driver) {
        WebElement navMenu = driver.findElement(By.id(navMenuId));
        List<WebElement> modules = TestUtils.getChildrenOfElement(navMenu, driver);

        for (WebElement moduleButton : modules) {
            if (moduleButton.getText().equals(moduleName)) {
                TestUtils.waitForVisibility(moduleButton, driver);
                moduleButton.click();
                break;
            }
        }
        Assert.assertTrue(TestUtils.waitForUrlToContain(urlModulePath, driver, 20),
                "URL should contain << " + urlModulePath + " >>, but current URL is: " + driver.getCurrentUrl());
    }

    public static EmployeePersonalDetailsPage goToPersonalDetailsModule(WebDriver driver) {
        clickModuleButton(Modules.PERSONAL_DETAILS, EmployeePersonalDetailsPage.urlModulePath[0], driver);
        return new EmployeePersonalDetailsPage(driver);
    }

    public static EmployeeContactDetailsPage goToContactDetails(WebDriver driver) {
        clickModuleButton(Modules.CONTACT_DETAILS, EmployeeContactDetailsPage.urlModulePath, driver);
        return new EmployeeContactDetailsPage(driver);
    }

    public static EmployeeJobPage goToJobModule(WebDriver driver) {
        clickModuleButton(Modules.JOB, EmployeeJobPage.urlModulePath, driver);
        return new EmployeeJobPage(driver);
    }

    public static EmployeeReportToPage goToReportToModule(WebDriver driver) {
        clickModuleButton(Modules.REPORT_TO, EmployeeReportToPage.urlModulePath, driver);
        return new EmployeeReportToPage(driver);
    }

}
