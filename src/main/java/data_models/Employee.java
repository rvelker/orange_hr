package data_models;

import java.util.Random;

public class Employee {

    public Employee(String firstName, String middleName, String lastName, boolean loginDetails) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.middleName = middleName;
        this.id = generateRandomEmployeeId(new Random());
        this.hasLoginDetails = loginDetails;
    }

    private String firstName = "";
    private String middleName = "";
    private String lastName = "";
    private String id = "";
    private String username = "";
    private String password = "";
    private boolean hasLoginDetails;
    private boolean statusEnabled;

    private String jobTitle = "";
    private String employmentStatus = "";
    private String subUnit = "";
    private String supervisor = "";

    private String generateRandomEmployeeId(Random random) {
        StringBuilder id = new StringBuilder();
        for (int i = 0; i < 6; i ++) { id.append(random.nextInt(10)); }
        return id.toString();
    }

    //region GETTERS
    public String getFirstName() {
        return firstName;
    }

    public String getMiddleName() {
        return middleName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getId() {
        return id;
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }

    public String getJobTitle() {
        return jobTitle;
    }

    public String getEmploymentStatus() {
        return employmentStatus;
    }

    public String getSubUnit() {
        return subUnit;
    }

    public boolean isStatusEnabled() {
        return statusEnabled;
    }

    public String getSupervisor() {
        return supervisor;
    }

    public boolean hasLoginDetails() {
        return hasLoginDetails;
    }
    //endregion

    //region SETTERS
    public void setJobTitle(String jobTitle) { this.jobTitle = jobTitle; }

    public void setEmploymentStatus(String employmentStatus) { this.employmentStatus = employmentStatus; }

    public void setSubUnit(String subUnit) { this.subUnit = subUnit; }

    public void setSupervisor(String supervisor) { this.supervisor = supervisor; }

    public void setUsername(String username) { this.username = username;}

    public void setPassword(String password) { this.password = password; }

    //endregion
}
