package data_models;

public class JobData {

    public static class JobTitles {
        public static final String ACCOUNT_ASSISTANT = "Account Assistant";
        public static final String CEO = "Chief Executive Officer";
        public static final String CFO = "Chief Financial Officer";
        public static final String CTO = "Chief Technical Officer";
        public static final String CONTENT_SPECIALIST = "Content Specialist";
        public static final String CUSTOMER_SUCCESS_MANAGER = "Customer Success Manager";
        public static final String DATABASE_ADMINISTRATOR = "Database Administrator";
        public static final String FINANCE_MANAGER = "Finance Manager";
        public static final String FINANCIAL_ANALYST = "Financial Analyst";
        public static final String HEAD_OF_SUPPORT = "Head of Support";
        public static final String HR_ASSOCIATE = "HR Associate";
        public static final String HR_MANAGER = "HR Manager";
        public static final String IT_MANAGER = "IT Manager";
        public static final String NETWORK_ADMINISTRATOR = "Network Administrator";
        public static final String PAYROLL_ADMINISTRATOR = "Payroll Administrator";
        public static final String PRE_SALES_COORDINATOR = "Pre-Sales Coordinator";
        public static final String QA_ENGINEER = "QA Engineer";
        public static final String QA_LEAD = "QA Lead";
        public static final String SALES_REPRESENTATIVE = "Sales Representative";
        public static final String SOCIAL_MEDIA_MARKETER = "Social Media Marketer";
        public static final String SOFTWARE_ARCHITECT = "Software Architect";
        public static final String SOFTWARE_ENGINEER = "Software Engineer";
        public static final String SUPPORT_SPECIALIST = "Support Specialist";
        public static final String VP_CLIENT_SERVICES = "VP - Client Services";
        public static final String VP_SALES_AND_MARKETING = "VP - Sales & Marketing";
    }

    public static class EmploymentStatus {
        public static final String FREELANCE = "Freelance";
        public static final String FULL_TIME_CONTRACT = "Full-Time Contract";
        public static final String FULL_TIME_PERMANENT = "Full-Time Permanent";
        public static final String FULL_TIME_PROBATION = "Full-Time Probation";
        public static final String PART_TIME_CONTRACT = "Part-Time Contract";
        public static final String PART_TIME_INTERNSHIP = "Part-Time Internship";
    }

    public static class SubUnit {
        public static final String ENGINEERING = "Engineering";
        public static final String ADMINISTRATION = "Administration";
        public static final String SALES_AND_MARKETING = "Sales & Marketing";
        public static final String CLIENT_SERVICES = "Client Services";
        public static final String FINANCE = "Finance";
        public static final String HR = "Human Resources";
    }
}
