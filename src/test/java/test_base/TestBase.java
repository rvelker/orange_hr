package test_base;

import org.openqa.selenium.By;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.ITestContext;
import org.testng.annotations.*;
import pages.DashboardPage;
import pages.LoginPage;
import utils.TestUtils;

import java.util.Arrays;

public class TestBase {

    public TestBase() { }
    protected static WebDriver driver;
    private static String driverPath = "src/test/drivers/";

    @Parameters({ "browserType"})
    @BeforeClass
    public void setupDriver(String browserType, ITestContext context) {
        try {
            setDriver(browserType);
            context.setAttribute("driver", driver);
        } catch (Exception e) {
            System.out.println("Error....." + Arrays.toString(e.getStackTrace()));
        }
    }

    @AfterClass
    public void tearDown() {
        driver.quit();
    }

    private void setDriver(String browserType) {
        if ("chrome".equalsIgnoreCase(browserType)) {
            driver = initChromeDriver();

        } else if ("firefox".equalsIgnoreCase(browserType)) {
            driver = initFirefoxDriver();

        } else {
            System.out.println("browser : " + browserType + " is invalid, Launching Chrome by default..");
            driver = initChromeDriver();
        }
    }

    private static WebDriver initChromeDriver() {
        System.out.println("Launching CHROME ...");
        System.setProperty("webdriver.chrome.driver", driverPath + "chromedriver.exe");
        WebDriver driver = new ChromeDriver();
        driver.manage().window().maximize();
        return driver;
    }

    private static WebDriver initFirefoxDriver() {
        System.out.println("Launching FIREFOX ...");
        System.setProperty("webdriver.gecko.driver", driverPath + "geckodriver.exe");
        WebDriver driver = new FirefoxDriver();
        driver.manage().window().maximize();
        return driver;
    }

    protected void logout() {
        WebDriverWait wait = new WebDriverWait(driver, 5);
        WebElement hamburgerMenu = wait.until(ExpectedConditions.elementToBeClickable(driver.findElement(By.id("welcome"))));
        By logoutButtonLocator = By.cssSelector("#welcome-menu > ul > li:nth-child(3) > a");

        do {
            hamburgerMenu.click();
        } while (!TestUtils.isClickable(driver.findElement(logoutButtonLocator), driver, 5));

        driver.findElement(logoutButtonLocator).click();
    }

    protected boolean currentUrlIs(String expected) {
        return TestUtils.waitForUrlToBe(expected, driver, 20);
    }

    protected boolean currentUrlContains(String string, int timeout) {
        return TestUtils.waitForUrlToContain(string, driver, timeout);
    }

    protected void sleep(long duration) {
        try {
            Thread.sleep(duration);
        }
        catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    protected void adminLogin() {
        LoginPage loginPage = new LoginPage(driver);
        driver.get(loginPage.url);
        loginPage.adminLogin();
        DashboardPage dashboardPage = new DashboardPage(driver);
        Assert.assertTrue(currentUrlIs(dashboardPage.url));
    }
}
