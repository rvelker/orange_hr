package test_suites.integration.addEmployee_employeeList;

import data_models.Employee;
import data_providers.NewEmployeeProvider;
import navigation.NavActions;
import org.openqa.selenium.By;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import pages.AddEmployeePage;
import pages.DashboardPage;
import pages.EmployeeListPage;
import pages.LoginPage;
import utils.TestUtils;

public class NewEmployeeWithLoginDetailsIsDisplayedInEmployeeListAndCanLogin extends Base {

    @BeforeMethod()
    public void login(){
        adminLogin();
    }

    @Test(dataProvider = "newEmployeesLoginDetails", dataProviderClass = NewEmployeeProvider.class)
    void on_new_employee_added_WITH_login_details_employee_is_displayed_in_employee_list(String firstName, String middleName, String lastName, String username, String password) {
        NavActions.goToAddEmployeePage(driver);
        AddEmployeePage addEmployeePage = new AddEmployeePage(driver);

        Employee employee = new Employee(firstName, middleName, lastName, true);
        employee.setUsername(username);
        employee.setPassword(password);
        addEmployeePage.addNewEmployee(employee);

        NavActions.goToEmployeeListPage(driver);
        EmployeeListPage employeeListPage = new EmployeeListPage(driver);
        employeeListPage.searchEmployeeById(employee.getId());

        Assert.assertTrue(TestUtils.waitForVisibility(driver.findElement(By.linkText(employee.getId())), driver).isDisplayed());
        assertCorrectEmployeeDetailsAreDisplayed(employee);

        // will not test login as login screen says 'account disabled' whenever account creation and login is attempted through automated script
//        logout();
//        LoginPage loginPage = new LoginPage(driver);
//        loginPage.login(employee.getUsername(), employee.getPassword());
//        Assert.assertTrue(currentUrlContains(DashboardPage.urlModulePath, 20), "Could not login with the new employee's login details");

    }
}
