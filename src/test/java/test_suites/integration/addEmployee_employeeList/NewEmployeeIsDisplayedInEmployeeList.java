package test_suites.integration.addEmployee_employeeList;

import data_models.Employee;
import data_providers.NewEmployeeProvider;
import navigation.NavActions;
import org.openqa.selenium.By;
import org.testng.Assert;
import org.testng.annotations.Test;
import pages.AddEmployeePage;
import pages.EmployeeListPage;
import utils.TestUtils;

public class NewEmployeeIsDisplayedInEmployeeList extends Base {

    @Test(dataProvider = "newEmployees", dataProviderClass = NewEmployeeProvider.class)
    void on_new_employee_added_WITHOUT_login_details_employee_is_displayed_in_employee_list(String firstName, String middleName, String lastName){
        NavActions.goToAddEmployeePage(driver);
        AddEmployeePage addEmployeePage = new AddEmployeePage(driver);

        Employee employee = new Employee(firstName, middleName, lastName,false);
        addEmployeePage.addNewEmployee(employee);

        NavActions.goToEmployeeListPage(driver);
        EmployeeListPage employeeListPage = new EmployeeListPage(driver);
        employeeListPage.searchEmployeeById(employee.getId());

        Assert.assertTrue(TestUtils.waitForVisibility(driver.findElement(By.linkText(employee.getId())), driver).isDisplayed());
        assertCorrectEmployeeDetailsAreDisplayed(employee);
    }

}
