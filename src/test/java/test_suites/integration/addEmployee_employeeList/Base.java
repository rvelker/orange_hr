package test_suites.integration.addEmployee_employeeList;

import data_models.Employee;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.annotations.*;
import org.testng.asserts.SoftAssert;
import pages.EmployeeListPage;
import test_base.TestBase;
import utils.TestUtils;

import java.util.List;

class Base extends TestBase {

    @BeforeMethod
    void setup(){
        adminLogin();
    }

    @AfterMethod
    void signOut(){
        logout();
    }

    protected void assertCorrectEmployeeDetailsAreDisplayed(Employee employee) {
        SoftAssert sAssert = new SoftAssert();

        WebElement idCell = TestUtils.getParentOfElement(driver.findElement(By.linkText(employee.getId())), driver);
        WebElement employeeRow = TestUtils.getParentOfElement(idCell, driver);
        List<WebElement> employeeCells = TestUtils.getChildrenOfElement(employeeRow, driver);

        sAssert.assertEquals(employeeCells.get(EmployeeListPage.TableColumns.ID).getText(), employee.getId(),
                "Value displayed in Employee ID cell does not match employee id");

        if (employee.getMiddleName().equals("")) {
            sAssert.assertEquals(employeeCells.get(EmployeeListPage.TableColumns.FIRST_NAME).getText(),
                    (employee.getFirstName()),
                    "Value displayed in Name column does not match employee first name");
        } else {
            sAssert.assertEquals(employeeCells.get(EmployeeListPage.TableColumns.FIRST_NAME).getText(),
                    employee.getFirstName() + " " + employee.getMiddleName(),
                    "Value displayed in Name column does not match employee first & middle name");
        }

        sAssert.assertEquals(employeeCells.get(EmployeeListPage.TableColumns.LAST_NAME).getText(),
                employee.getLastName(),
                "Value displayed in Name column does not match expected");

        sAssert.assertEquals(employeeCells.get(EmployeeListPage.TableColumns.JOB).getText(),
                "",
                "Value displayed in Job column should be blank");

        sAssert.assertEquals(employeeCells.get(EmployeeListPage.TableColumns.STATUS).getText(),
                "",
                "Value displayed in Employment column should be blank");

        sAssert.assertEquals(employeeCells.get(EmployeeListPage.TableColumns.SUB_UNIT).getText(),
                "",
                "Value displayed in Sub Unit column should be blank");

        sAssert.assertEquals(employeeCells.get(EmployeeListPage.TableColumns.SUPERVISOR).getText(),
                "",
                "Value displayed in Supervisor column should be blank");
        sAssert.assertAll();
    }

}
