package test_suites.integration.editPersonalDetails_employeeList;

import data_models.Employee;
import navigation.NavActions;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.asserts.SoftAssert;
import pages.AddEmployeePage;
import pages.EmployeeListPage;
import test_base.TestBase;
import utils.TestUtils;

import java.util.List;
import java.util.Random;

class Base extends TestBase {

    @BeforeMethod
    void setup() {
        adminLogin();
        NavActions.goToAddEmployeePage(driver);
    }

    @AfterMethod
    void signOut() {
        logout();
    }

    protected void assertCorrectEmployeeDetailsAreDisplayed(Employee employee) {
        SoftAssert sAssert = new SoftAssert();

        WebElement idCell = TestUtils.getParentOfElement(driver.findElement(By.linkText(employee.getId())), driver);
        WebElement employeeRow = TestUtils.getParentOfElement(idCell, driver);
        List<WebElement> employeeCells = TestUtils.getChildrenOfElement(employeeRow, driver);

        sAssert.assertEquals(employeeCells.get(EmployeeListPage.TableColumns.ID).getText(), employee.getId(),
                "Value displayed in Employee ID column does not match employee id");

        if (employee.getMiddleName().equals("")) {
            sAssert.assertEquals(employeeCells.get(EmployeeListPage.TableColumns.FIRST_NAME).getText(),
                    (employee.getFirstName()),
                    "Value displayed in Name column does not match employee first name");
        } else {
            sAssert.assertEquals(employeeCells.get(EmployeeListPage.TableColumns.FIRST_NAME).getText(),
                    employee.getFirstName() + " " + employee.getMiddleName(),
                    "Value displayed in Name column does not match employee first & middle name");
        }

        sAssert.assertEquals(employeeCells.get(EmployeeListPage.TableColumns.LAST_NAME).getText(),
                employee.getLastName(),
                "Value displayed in Last Name column does not match expected");

        sAssert.assertEquals(employeeCells.get(EmployeeListPage.TableColumns.JOB).getText(),
                employee.getJobTitle(),
                "Value displayed in Job column does not match expected");

        sAssert.assertEquals(employeeCells.get(EmployeeListPage.TableColumns.STATUS).getText(),
                employee.getEmploymentStatus(),
                "Value displayed in Employment does not match expected");

        sAssert.assertEquals(employeeCells.get(EmployeeListPage.TableColumns.SUB_UNIT).getText(),
                employee.getSubUnit(),
                "Value displayed in Sub Unit column does not match expected");

        sAssert.assertEquals(employeeCells.get(EmployeeListPage.TableColumns.SUPERVISOR).getText(),
                employee.getSupervisor(),
                "Value displayed in Supervisor column does not match expected");
        sAssert.assertAll();
    }

    protected void assertCorrectSupervisorNameIsDisplayed(String supervisorName, String id){
        WebElement idCell = TestUtils.getParentOfElement(driver.findElement(By.linkText(id)), driver);
        WebElement employeeRow = TestUtils.getParentOfElement(idCell, driver);
        List<WebElement> employeeCells = TestUtils.getChildrenOfElement(employeeRow, driver);
        Assert.assertEquals(employeeCells.get(EmployeeListPage.TableColumns.SUPERVISOR).getText(), supervisorName,
                "Value displayed in Supervisor column does not match expected");
    }

    protected String getOtherRandomEmployeeFromList(EmployeeListPage page, String employeeId) {
        WebElement table = TestUtils.waitForVisibility(page.tblEmployees, driver);
        List<WebElement> employees = TestUtils.getChildrenOfElement(table, driver);
        return getRandomEmployeeName(employees, employeeId);
    }

    private String getRandomEmployeeName(List<WebElement> employees, String id){
        Random random = new Random();
        int randomIndex = random.nextInt(employees.size());

        WebElement randomEmployeeTableRow = employees.get(randomIndex);
        List<WebElement> cells = TestUtils.getChildrenOfElement(randomEmployeeTableRow, driver);
        String employeeId = cells.get(EmployeeListPage.TableColumns.ID).getText();

        if (employeeId.equals(id)){
            getRandomEmployeeName(employees, id);
        }

        return cells.get(EmployeeListPage.TableColumns.FIRST_NAME).getText() + " " + cells.get(EmployeeListPage.TableColumns.LAST_NAME).getText();
    }



}
