package test_suites.integration.editPersonalDetails_employeeList;

import data_models.Employee;
import data_providers.NewEmployeeProvider;
import navigation.NavActions;
import navigation.NavEmployeeDetails;
import org.testng.annotations.Test;
import pages.AddEmployeePage;
import pages.EmployeeListPage;
import pages.employee_details.EmployeeReportToPage;

public class SupervisorPersonalDetailsUpdatesAreDisplayedInEmployeeList extends Base {

    @Test(dataProvider = "newEmployees", dataProviderClass = NewEmployeeProvider.class)
    void personal_details_supervisor_module_updates_are_displayed_in_employee_list(String firstName, String middleName, String lastName) {
        AddEmployeePage addEmployeePage = new AddEmployeePage(driver);
        Employee employee = new Employee(firstName, middleName, lastName, false);
        addEmployeePage.addNewEmployee(employee);
        String employeeId = employee.getId();

        NavActions.goToEmployeeListPage(driver);
        EmployeeListPage employeeListPage = new EmployeeListPage(driver);
        String supervisorName = getOtherRandomEmployeeFromList(employeeListPage, employeeId);
        employeeListPage.searchEmployeeById(employee.getId());

        employeeListPage.openEmployeeDetailsPage(employee.getId());

        EmployeeReportToPage reportToPage = NavEmployeeDetails.goToReportToModule(driver);

        reportToPage.clickAddSupervisor();
        reportToPage.saveSupervisor(supervisorName, EmployeeReportToPage.ReportingMethods.DIRECT);
        employee.setSupervisor(supervisorName);

        NavActions.goToEmployeeListPage(driver);
        employeeListPage.searchEmployeeById(employee.getId());
        assertCorrectEmployeeDetailsAreDisplayed(employee);
    }

}
