package test_suites.integration.editPersonalDetails_employeeList;

import data_models.Employee;
import data_models.JobData;
import data_providers.NewEmployeeProvider;
import navigation.NavActions;
import navigation.NavEmployeeDetails;
import org.openqa.selenium.By;
import org.testng.Assert;
import org.testng.annotations.Test;
import pages.AddEmployeePage;
import pages.EmployeeListPage;
import pages.employee_details.EmployeeJobPage;
import utils.TestUtils;

public class JobPersonalDetailsUpdatesAreDisplayedInEmployeeList extends Base {

    @Test(dataProvider = "newEmployees", dataProviderClass = NewEmployeeProvider.class)
    void personal_details_job_module_updates_are_displayed_in_employee_list(String firstName, String middleName, String lastName) {
        AddEmployeePage addEmployeePage = new AddEmployeePage(driver);
        Employee employee = new Employee(firstName, middleName, lastName, false);
        addEmployeePage.addNewEmployee(employee);

        NavActions.goToEmployeeListPage(driver);
        EmployeeListPage employeeListPage = new EmployeeListPage(driver);
        employeeListPage.searchEmployeeById(employee.getId());
        employeeListPage.openEmployeeDetailsPage(employee.getId());

        EmployeeJobPage jobPage =  NavEmployeeDetails.goToJobModule(driver);

        jobPage.edit();

        jobPage.selectEmploymentStatus(JobData.EmploymentStatus.FULL_TIME_PERMANENT);
        employee.setEmploymentStatus(JobData.EmploymentStatus.FULL_TIME_PERMANENT);

        jobPage.selectJobTitle(JobData.JobTitles.SUPPORT_SPECIALIST);
        employee.setJobTitle(JobData.JobTitles.SUPPORT_SPECIALIST);

        jobPage.selectSubUnit(JobData.SubUnit.CLIENT_SERVICES);
        employee.setSubUnit(JobData.SubUnit.CLIENT_SERVICES);

        employee.setSupervisor("");

        jobPage.save();

        NavActions.goToEmployeeListPage(driver);
        employeeListPage.searchEmployeeById(employee.getId());

        Assert.assertTrue(TestUtils.waitForVisibility(driver.findElement(By.linkText(employee.getId())), driver).isDisplayed());
        assertCorrectEmployeeDetailsAreDisplayed(employee);
    }




}
