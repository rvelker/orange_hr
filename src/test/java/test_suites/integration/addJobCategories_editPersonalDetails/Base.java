package test_suites.integration.addJobCategories_editPersonalDetails;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import pages.EmployeeListPage;
import pages.employee_details.EmployeeJobPage;
import test_base.TestBase;
import utils.TestUtils;

import java.util.List;
import java.util.Random;

class Base extends TestBase {

    EmployeeListPage employeeListPage;

    @BeforeMethod
    void setup() {
        adminLogin();
        employeeListPage = new EmployeeListPage(driver);
    }

    @AfterMethod
    void signOut() {
        logout();
        driver.manage().deleteAllCookies();
    }

    protected void openRandomEmployeePersonalDetailsPage() {
        Random random = new Random();
        WebElement table = TestUtils.waitForVisibility(employeeListPage.tblEmployees, driver);
        List<WebElement> employees = TestUtils.getChildrenOfElement(table, driver);
        int randomIndex = random.nextInt(employees.size());

        WebElement randomEmployee = employees.get(randomIndex);
        List<WebElement> randomEmployeeCells = TestUtils.getChildrenOfElement(randomEmployee, driver);
        WebElement randomEmployeeIdAnchorTag = TestUtils.waitForClickable(randomEmployeeCells.get(EmployeeListPage.TableColumns.ID), driver);
        employeeListPage.openEmployeeDetailsPage(randomEmployeeIdAnchorTag.getText());
    }

    protected void assertNewJobIsPresentInJobList(EmployeeJobPage jobPage, String jobTitle){
        WebElement jobDropdown = TestUtils.waitForClickable(jobPage.ddlJobTitle, driver);

        Select select = new Select(jobDropdown);
        List<WebElement> jobs = select.getOptions();

        boolean jobIsPresent = false;

        for (WebElement job : jobs) {
            if (job.getText().equals(jobTitle)) {
                jobIsPresent = true;
                break;
            }
        }
        Assert.assertTrue(jobIsPresent);
    }
}
