package test_suites.integration.addJobCategories_editPersonalDetails;

import data_providers.JobCategoriesProvider;
import data_providers.NewEmployeeProvider;
import navigation.NavActions;
import navigation.NavEmployeeDetails;
import org.testng.annotations.Test;
import pages.admin_job.jobTitles.AdminJobTitlesPage;
import pages.admin_job.jobTitles.AdminSaveJobTitlesPage;
import pages.employee_details.EmployeeJobPage;

public class NewJobTitlesAreDisplayedInPersonalDetailsPage extends Base {

    @Test(dataProvider = "newJobTitles", dataProviderClass = JobCategoriesProvider.class)
    void on_new_job_title_added_title_can_be_selected_in_employee_details_page(String jobTitle) {
        NavActions.goToAdminJobTitles(driver);
        AdminJobTitlesPage titlesPage = new AdminJobTitlesPage(driver);

        AdminSaveJobTitlesPage saveTitlePage = titlesPage.addJob();
        saveTitlePage.saveJob(jobTitle);

        NavActions.goToEmployeeListPage(driver);
        openRandomEmployeePersonalDetailsPage();

        EmployeeJobPage jobPage = NavEmployeeDetails.goToJobModule(driver);
        jobPage.edit();
        assertNewJobIsPresentInJobList(jobPage, jobTitle);
    }

}
