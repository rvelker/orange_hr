package test_listeners;

import org.testng.IRetryAnalyzer;
import org.testng.ITestResult;

import java.util.Arrays;

public class RetryAnalyzer implements IRetryAnalyzer {

    private int count = 0;

    @Override
    public boolean retry(ITestResult result) {
        int maxCount = 2;

        if (!result.isSuccess()) {
            if (count < maxCount) {
                count++;
                System.out.println(Arrays.toString(result.getThrowable().getStackTrace()));
                result.setStatus(ITestResult.FAILURE);
                return true;
            } else {
                result.setStatus(ITestResult.FAILURE);
            }
        } else {
            result.setStatus(ITestResult.SUCCESS);
        }
        return false;
    }
}