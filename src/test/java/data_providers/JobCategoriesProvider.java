package data_providers;

import data_providers.csv_reader.CSVReader;
import org.testng.annotations.DataProvider;

import java.io.IOException;

public class JobCategoriesProvider {

    @DataProvider(name = "existingJobTitle")
    public static Object[][] existingJobTitle() throws IOException {
        return CSVReader.readRandomRows("D:\\IntelliJ Projects\\OrangeHR\\src\\test\\resources\\data\\job_categories\\existing\\JobTitles.csv",1);

    }

    @DataProvider(name = "newJobTitles")
    public static Object[][] newJobTitles() throws IOException {
        return CSVReader.readRandomRows("D:\\IntelliJ Projects\\OrangeHR\\src\\test\\resources\\data\\job_categories\\new\\NewJobTitles.csv",3);
    }

}
