package data_providers.csv_reader;

import java.io.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

public class CSVReader {

    public static String[][] readRandomRows(String file, int rows) throws IOException {
        int totalRows = countRows(file);
        List<Integer> indexList = getRandomIndexList(rows, totalRows);
        BufferedReader br = new BufferedReader(new FileReader(new File(file)));

        int columns = br.readLine().split(",").length;
        String[][] values = new String[rows][columns];
        int linesRead = 0;

        for (int i = 0; i < rows; i++) {

            for (int j = linesRead; j < totalRows; j++) {

                String line = br.readLine();

                if (indexList.contains(linesRead)) {
                    linesRead++;
                    values[i] = line.split(",");
                    break;
                }
                linesRead++;
            }
        }
        br.close();
        return values;
    }

    private static int countRows(String filename) throws IOException {
        try (InputStream is = new BufferedInputStream(new FileInputStream(filename))) {
            byte[] c = new byte[1024];
            int count = 0;
            int readChars = 0;
            boolean empty = true;
            while ((readChars = is.read(c)) != -1) {
                empty = false;
                for (int i = 0; i < readChars; ++i) {
                    if (c[i] == '\n') {
                        ++count;
                    }
                }
            }
            is.close();
            return (count == 0 && !empty) ? 1 : count;
        }
    }

    private static List<Integer> getRandomIndexList(int rowsNeeded, int totalRows) {
        List<Integer> indexList = new ArrayList<>();
        Random rand = new Random();

        for (int i = 0; i < rowsNeeded; i++) {
            int randomIndex = rand.nextInt(totalRows);

            if (!indexList.contains(randomIndex)) {
                indexList.add(randomIndex);
            } else {
                i--;
            }
        }
        Collections.sort(indexList);
        return indexList;
    }
}
