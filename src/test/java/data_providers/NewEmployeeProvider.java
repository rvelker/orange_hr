package data_providers;

import data_providers.csv_reader.CSVReader;
import org.testng.annotations.DataProvider;

import java.io.IOException;

public class NewEmployeeProvider {

    @DataProvider(name = "newEmployees")
    public static Object[][] newEmployees() throws IOException {
        return CSVReader.readRandomRows("D:\\IntelliJ Projects\\OrangeHR\\src\\test\\resources\\data\\new_employees\\NewEmployeeNames.csv",3);
    }

    @DataProvider(name = "newEmployeesLoginDetails")
    public static Object[][] newEmployeesLoginDetails() throws IOException {
        return CSVReader.readRandomRows("D:\\IntelliJ Projects\\OrangeHR\\src\\test\\resources\\data\\new_employees\\NewEmployeeNamesWithLoginDetails.csv",3);
    }


}
