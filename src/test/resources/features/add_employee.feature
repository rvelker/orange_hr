Feature: Add New Employee

  Rule: Admin can add and save new employees to the system and optionally create login credentials for them

    Background:
      Given Browser is open
      And User navigates to Login Page
      And User logs in as admin

    Scenario: Add new employee and create login details
      Given User navigates to Add Employee Page

      When User types in new employee's details
      And User types in new employee's <username> and <password>
      And User saves the new employee

      Then System displays new employee's Personal Details Page
      And Employee List Page displays new employee
      And User logs in with <username> and <password>

    Scenario: Add new employee without creating login details
      Given User navigates to Add Employee Page

      When User types in new employee's details
      And User saves new employee

      Then System displays the new employee's profile
      And 'Employee List' page displays new employee

